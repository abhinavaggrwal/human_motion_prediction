# Machine Perception Project

## Introduction

This project uses implementation inspired by Martinez et al. for Human motion prediction task (Project 4)

## Running the code

In order to run the code, use following command from directory where all the code files are present. We are assuming virtual environment setup is same as mentioned on project website 
```
python train.py --data_dir <path to data directory> --save_dir ./experiments_final --experiment_name final --cell_size 512 --num_layers 3 --num_epochs 200 --learning_rate 0.001 --cell_type lstm --batch_size 16 --seq_length_in 50
```
## Evaluating the code
In order to evaluate the code, use the following command from directory where all the code files are present.
```
python evaluate_test.py --data_dir <path to data directory> --save_dir ./experiments_final --model_id <model_timestamp> --export
```